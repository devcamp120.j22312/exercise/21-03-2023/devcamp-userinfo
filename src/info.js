import avatar from "./assets/images/avatar.png"

const gUserInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: avatar,
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
  }

export { gUserInfo };